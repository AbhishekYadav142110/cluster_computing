'''
'MPI' for interprocess communication
'json' for loading json files
'time' for recording execution time
'argparse' for parsing filenames passed in command line  
're' for splitting tweets on multiple punctuations
'''
from mpi4py import MPI
import json
import time
import argparse
import re

'''
Parse json filename from the command line

'''
parser = argparse.ArgumentParser()
parser.add_argument("twitter_file", help="twitter json file to be processed")
parser.add_argument("coordinates_file", help="json file having grid coordinates")
parser.add_argument("words_file", help="text file having words and their respective scores")
args = parser.parse_args()


'''
my_rank <- get current process rank
p <- total number of processes

'''
comm = MPI.COMM_WORLD
my_rank = comm.Get_rank()
p = comm.Get_size()

'''
data <- processes store their respective chunks from the whole data
grids <- melbGrid.json data
zones <- data refined from 'grids' variable; only stores grid id and its coordinates
word_scores <- data from AFINN.txt 
extremes <- extreme values of the the 4 coordinates 
count <- processes store their respective results

'''

data = []
grids = None
zones = []
word_scores = {}
extremes = {'ymax': None, 'ymin': None, 'xmax': None, 'xmin': None}
count = {}

start_time = None
end_time = None


def initialize_count(zones):
    '''
    This function initialises the count variable with 0's for 
    every grid; invoked by every process at the start.
    '''
    for i in range(len(zones)):
        count[zones[i]['id']] = {'tweets': 0, 'sentiment score': 0}


def calculate_adjusted_scores():
    '''
    Master uses this function to adjust scores for keys in word_scores
    which have more than one word like 'dont like' to handle word match overlap.
    '''
    for word in word_scores:
        split_words = word.split()
        if len(split_words)>1:
            for subword in split_words:
                if subword in word_scores:
                    word_scores[word] = word_scores[word] - word_scores[subword]

                    
'''
If the executing process is master process (my_rank=0):
1. word_scores <- load the AFINN.txt data in a dictionary
2. Load the grid file
3. zones <- store the grid coordinates along with their ids

'''
if my_rank==0:
    start_time = time.time()
    with open(args.words_file) as f:
        for i, line in enumerate(f):
            e = line.strip().split("\t")
            word_scores[" ".join(e[0:-1])] = int(e[-1])
    calculate_adjusted_scores()
    print(f"Word scores loaded in process {my_rank}")
    with open(args.coordinates_file, encoding="utf8") as f:
        grids = json.load(f)
    extremes['ymax'] = grids['features'][0]['properties']['ymax']
    extremes['ymin'] = grids['features'][0]['properties']['ymin']
    extremes['xmax'] = grids['features'][0]['properties']['xmax']
    extremes['xmin'] = grids['features'][0]['properties']['xmin']
    for i in range(len(grids['features'])):
        zones.append(grids['features'][i]['properties'])
        if grids['features'][i]['properties']['ymax'] > extremes['ymax']:
            extremes['ymax'] = grids['features'][i]['properties']['ymax']
        if grids['features'][i]['properties']['ymin'] < extremes['ymin']:
            extremes['ymin'] = grids['features'][i]['properties']['ymin']
        if grids['features'][i]['properties']['xmax'] > extremes['xmax']:
            extremes['xmax'] = grids['features'][i]['properties']['xmax']
        if grids['features'][i]['properties']['xmin'] < extremes['xmin']:
            extremes['xmin'] = grids['features'][i]['properties']['xmin']
    print(f"Grid cordinates loaded in process {my_rank}")
    initialize_count(zones)




def is_contained(coordiantes, extremes):
    '''
    This function checks if a particular tweet is located within
    any of the the given grids or not using extremes. 
    '''
    x, y = coordiantes
    if x>=extremes['xmin'] and x<=extremes['xmax'] and y>=extremes['ymin'] and y<=extremes['ymax']:
        return True
    else:
        return False


def extract_data(extremes):
    '''
    Function for extracting only necessary data (according to my_rank) 
    for a particular process from the whole data.
    Example: If there are 4 processes, process number 2 extracts record number 2, 6, 10, ... and so on
    '''
    print(f"Process {my_rank} inside extract_data()")
    line_count = -1

    with open(args.twitter_file, encoding="utf8") as f:
        for _, line in enumerate(f):
            if line.startswith('{"id"'):
                line_count = line_count + 1

                coordinates_index = line.find('"coordinates":[')
                coordinates_end_index = line.find("]},", coordinates_index)
                coordinates_string = line[coordinates_index + 15:coordinates_end_index]
                coordinates_sep = coordinates_string.find(",")
                x = float(coordinates_string[:coordinates_sep])
                y = float(coordinates_string[coordinates_sep+1:])

                text_index = line.find('"text":')
                text_end_index = line.find('",', text_index)
                text = line[text_index + 8: text_end_index]
                
                if (line_count - my_rank) % p == 0 and is_contained((x, y), extremes):
                    data.append({"text": text, "coordinates": [x, y]})
    
    print(f"I am process {my_rank} and I filtered {len(data)} records")



def aggregate_counts(data):
    '''
    Function executed by the master (my_rank=0) for aggregating 
    values of count from the worker processes
    '''
    print(f"Process {my_rank} inside aggregate_counts()")
    process_count = data

    for key in process_count.keys():
        count[key]['tweets'] = count[key]['tweets'] + process_count[key]['tweets']
        count[key]['sentiment score'] = count[key]['sentiment score'] + process_count[key]['sentiment score']


def calculate_average():
    '''
    The master uses this function to calculate average sentiment score per tweet per grid.
    '''
    for key in count.keys():
        if count[key]['tweets']>0:
            count[key]['average score'] = count[key]['sentiment score'] / count[key]['tweets']
        else:
            count[key]['average score'] = 0


def display_result():
    '''
    The master uses this function to display the final result.
    '''
    print("---------------------------------------------------------")
    print("Cell\t#Total Tweets\t#Overall Sentiment Score\t#Average Sentiment Score")
    for key in count.keys():
        print(f"{key}\t{count[key]['tweets']}\t{count[key]['sentiment score']}\t{count[key]['average score']}")
    print("---------------------------------------------------------")


def calculate_tweet_score(split_text, word_scores):
    '''
    This function checks for a word match. A word followed by an indefinite string 
    of the below mentioned punctuation marks is also a match. 
    '''

    score = 0
    for ele in split_text:
        if ele in word_scores:
            score = score + word_scores[ele]
    return score


def get_counts(zones, word_scores):
    '''
    Every worker process calls this function on its chunk  and gets its result.
    This function loops through the data and for every record:
    1. Checks which grid this data belongs to
    2. Increments the tweet count of that grid and
    3. Calculates the sentiment score for the current record by
       looping through all the words defined in the AFINN file and checking if its exists 
       in this record along with acceptable suffixes

    '''
    print(f"Process {my_rank} inside get_counts()")
    initialize_count(zones)

    for i in range(len(data)):
        x = data[i]['coordinates'][0]
        y = data[i]['coordinates'][1]
        text = data[i]['text'].lower()
        text = re.split('[\s!,.?\'"]', text)
        text = " ".join(text).split()

        for j in range(len(zones)):
            if zones[j]['xmin']< x and zones[j]['xmax']>=x and zones[j]['ymin']< y and zones[j]['ymax']>=y:
                count[zones[j]['id']]['tweets'] = count[zones[j]['id']]['tweets'] + 1
                for entry_length in range(1, 4):
                    paired_text = []
                    if entry_length > 1:
                        for i in range(len(text) - entry_length + 1):
                            paired_text.append(" ".join(text[i:i+entry_length]))
                    
                    current_tweet = None
                    if entry_length>1:
                        current_tweet = paired_text
                    else:
                        current_tweet = text
                    
                    text_score = calculate_tweet_score(current_tweet, word_scores)

                    count[zones[j]['id']]['sentiment score'] = count[zones[j]['id']]['sentiment score'] + text_score

              
    

'''
    Master broadcasts common data i.e. grid coordinates, word scores, 
    and extremes to all workers

'''

zones, word_scores, extremes = comm.bcast((zones, word_scores, extremes))
worker_start = time.time()
extract_data(extremes)
worker_mid = time.time()
get_counts(zones, word_scores)
worker_end = time.time()
if my_rank!=0:
    comm.send((count, worker_end - worker_start, worker_mid - worker_start, worker_end - worker_mid), dest = 0)

if my_rank == 0:
    if p > 1:
        '''
        Master aggregates the results from workers

        '''
        for proc_id in range(1, p):
            print(f'Data received from {proc_id}')
            process_work, worker_time, ext_time, cal_time = comm.recv(source = proc_id)
            print(f'Process {proc_id} executed in {worker_time} seconds: Extraction: {ext_time}, Calculation: {cal_time}')
            aggregate_counts(process_work)
    end_time = time.time()    

    print(f'Master executed in {worker_end - worker_start} seconds: Extraction: {worker_mid - worker_start}, Calculation: {worker_end - worker_mid}')
    print(f'Aggregated counts in time = {str(end_time - start_time)}')
    calculate_average()
    display_result()


